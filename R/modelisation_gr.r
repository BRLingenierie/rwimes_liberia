# use roxygen2:
# https://cran.r-project.org/web/packages/roxygen2/vignettes/rd.html
# run roxygen2::roxygenise() to generate documentation
#url = "https://wimes-liberia-undp.brl.fr"
#key = "da0d8737-6f86-426a-80d8-04dfd60b341c"
#wimesInitialization(url, key)
# https://wimes-liberia-undp.brl.fr/scripting/updateRPackages
#' Run the GR4J model for Liberia basins
#' Revivised by Sothea on 27/07/2020 
#' 
#' @export
#' @param bvCode - code of the polygon site that represents the watershed of the prevision point
#' @param ffcode - code of the timeseries which will recieve the forecast flow 
#' @param rfcode - code of the timeseries which will recieve the forecast rainfal
#' @param gridSiteCode_rf - code of the timeseries which contains the forecast rainfall as grid
#' @param gridSiteCode_ef - code of the timeseries which contains the forecast etp as grid
#' @param productionStore_GR4 - code of a intenal timeseries for real-time calculation
#' @param routingStore_GR4 - code of a intenal timeseries for real-time calculation
#' @param soilCapacityStore_x1 - parameter of GR4J model x1
#' @param waterExchageCoef_x2 - parameter of GR4J model x2
#' @param routingStoreCapacity_x3 - parameter of GR4J model x3
#' @param timeUnitHydrograph_x4 - parameter of GR4J model x4
#' @param basinArea - basin area in square kilometers
#' @param gridSiteCode_ro - code of the timeseries which contains the observed rainfall as grid
#' @param foCode - code of the timeseries which will recieve the observed flow
#' @param roCode - code of the timeseries which will recieve the observed precipitation
#'
airgr_wimes_liberia = function(bvCode, 
                               ffCode, 
                               rfCode, 
                               gridSiteCode_rf,
                               gridSiteCode_ef,
                               productionStore_GR4, 
                               routingStore_GR4, 
                               soilCapacityStore_x1, 
                               waterExchageCoef_x2, 
                               routingStoreCapacity_x3, 
                               timeUnitHydrograph_x4, 
                               basinArea, 
                               gridSiteCode_ro, 
                               foCode, 
                               roCode,
                               silent = TRUE) {
  

  ################################# 1 - Library loading & date format  ##########################################
  logInfo("STEP 1: Loading libraries")
      
  library(airGR)
  library(rwimes)
  library(geojsonR)
  library(rgdal)
  library(raster)
      
  #################################### 2 - Basin rainfall calculating ############################################
  logInfo("STEP 2: Calculating basin rainfall")
  tryCatch({  
    # Returns the geometry of the site as a GDAL polygon
    logInfo("--- Getting the geometry of the site as a GDAL polygon")
    bv = GetGeometry(bvCode)
    
    # Calculates basin forecast raifall
    logInfo("--- Calculating basin forecast raifall ")
    rf_bv_d = CalculateForecastRainfallBasin(gridSiteCode_rf, bv)

    # Calculates observed basin rainfall
    logInfo("--- Calculating basin observed raifall")
    ro_bv_d = CalculateObservedRainfallBasin(gridSiteCode_ro, bv)
    
  },
  error = function(e) {
    logError(e$message)
    stop()
  }) 

  ##################################################### 3 - ETP FORECAST ##################################################
  logInfo("STEP 3: ETP FORECAST")
  tryCatch({
    # Calculates basin forecast etp
    logInfo("--- Calculating basin forecast etp ")
    ef_bv_d = CalculateForecastEtpBasin(gridSiteCode_ef, bv)
  },
  error = function(e) {
    logError(e$message)
    stop()
  }) 
  
  ##################################################### 4 - ETP OBSERVED ##################################################
  logInfo("STEP 4: ETP observed")
  tryCatch({
    # Reads ETP paramters
    logInfo("--- Reading ETP parameters")
    params = etpParameters()
    
    logInfo("--- Assigning ETP parameters")
    meanETPDay_Date = params[[1]]
    meanETPDay = params[[2]]
    
    # Generates ETP dates following rainfall dates
    etpDateDay_obs = paste(substr(ro_bv_d[,1], 9, 10), "/",
                       substr(ro_bv_d[,1], 6, 7), 
                       sep = "")
    etpDate_obs = rep(0, length(ro_bv_d$Date))
    
    # Calculate ETP in forecast period
    for (i in seq(along = etpDate_obs)) {
      etpDate_obs[i] = meanETPDay[which(meanETPDay_Date == etpDateDay_obs[i])]
    }
  },
  error = function(e) {
    logError(e$message)
    stop()
  }) 
  
  ##################################################### 5 - Inititialising S0 and R0  ##################################################
  logInfo("STEP 5: Inititialising S0 and R0")
  
  ### getting initial production store 
  initialProductionStore = initialProductionStoreGetting(ro_bv_d[1,1],
                                                         productionStore_GR4)
  ### getting initial routing store
  initialRoutingStore = initialRoutingStoreGetting(ro_bv_d,
                                                   initialProductionStore, 
                                                   routingStore_GR4, 
                                                   etpDate_obs, 
                                                   soilCapacityStore_x1, 
                                                   waterExchageCoef_x2, 
                                                   routingStoreCapacity_x3, 
                                                   timeUnitHydrograph_x4, 
                                                   basinArea,
                                                   foCode)
  
  ####################################### 6 - Combinding observed/forcast data ############################################
  logInfo("STEP 6: Combinding observed/forcast data")
  ### rainfall : 10 observed rainfall + 17 forcasted rainfall
  ro_bv_d[1,2] = 0
  rof_bv_d = rbind(ro_bv_d, rf_bv_d) # 
  
  ### etp : 10 observed etp + 16 forcasted etp
  etpDate_obsFor = c(etpDate_obs, ef_bv_d[ , 2]) 
  
  ####################################### 7 - Flow simulation ############################################
  logInfo("STEP 7: Running GR4J")
  tryCatch({
    gr4JResult = GR4JSimulation(rof_bv_d, 
                                initialProductionStore, 
                                initialRoutingStore, 
                                etpDate_obsFor, 
                                soilCapacityStore_x1, 
                                waterExchageCoef_x2, 
                                routingStoreCapacity_x3, 
                                timeUnitHydrograph_x4, 
                                basinArea)
    observedFlow = gr4JResult[[1]][1:10] # first 10 values are observed flow
    forcastedFlow = gr4JResult[[1]][10:26] # last 17 values are forcasted flow
    productionStoreOut = gr4JResult[[2]][1:10, ]
    # keep same initialProductionStore
    productionStoreOut[1, 2] = initialProductionStore
    
    routingStoreOut = gr4JResult[[3]][1:10, ]
  },
  error = function(e) {
    logError(e$message)
    stop()
  })
 
  ############################################ 8 - Results writting  ###############################################
  logInfo("STEP 8: Writting results")
      
  # production date = Now
  productionDate = Sys.time()
  
  tryCatch({
 
      # water content in the production store
      logInfo("Upserting water content in the production store parameter")
      upsertQuantValues( data.frame(timeSeriesCode = productionStore_GR4, 
                                    observationDateTime = as.POSIXct(productionStoreOut[, 1], tz = "UTC"),
                                    value = productionStoreOut[, 2]))
            
      # water content in the routing store
      logInfo("Upserting water content in the routing store parameter")
      upsertQuantValues( data.frame(timeSeriesCode = routingStore_GR4, 
                                    observationDateTime = as.POSIXct(routingStoreOut[, 1], tz = "UTC"),
                                    value = routingStoreOut[, 2]))
            
      # forcasted flow
      upsertQuantForecastValues(  data.frame(timeSeriesCode = ffCode,
                                             productionDateTime = as.POSIXct(productionDate, tz = "UTC"),
                                             forecastDateTime = as.POSIXct(rof_bv_d[10:26, 1], tz = "UTC"),
                                             value = forcastedFlow))
            
      # forcasted rainfall
      logInfo("Upserting the forcasted rainfall")
      upsertQuantForecastValues( data.frame( timeSeriesCode = rfCode, 
                                             productionDateTime = as.POSIXct(productionDate, tz = "UTC"), 
                                             forecastDateTime =  as.POSIXct(rof_bv_d[10:26, 1], tz = "UTC"),
                                             value = rof_bv_d[10:26, 2]))
     # observed flow  
    logInfo(paste0("Upserting the observed flow"))
    if (length(observedFlow) != length(ro_bv_d$Date)) {
      stop("observedFlow and ro_bv_d$Date have not the same length")
    }
    if (!is.null(observedFlow)) {
      upsertQuantValues(data.frame( timeSeriesCode =  foCode, 
                                    observationDateTime = as.POSIXct(ro_bv_d$Date, tz = "UTC"),
                                    value = observedFlow))
    }
        
    # the observed rainfall is at d-1 23h 30, we consider then at d 00h 00
    logInfo(paste0("Upserting the observed rainfall"))
    if (!is.null(ro_bv_d$Date)) {
      upsertQuantValues(data.frame(timeSeriesCode = roCode, 
                                   observationDateTime = as.POSIXct(ro_bv_d[ , 1], tz = "UTC"), 
                                   value = ro_bv_d$`Rainfall (mm/d)`))
    }
  },
  error = function(e) {
    #logError(e$message)
    stop(e$message)
  })   
  ############################################ END  ###############################################
  logInfo("END")
}